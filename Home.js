import React, { Component } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class Home extends Component {

    componentDidMount() {
        console.log('Cek props', this.props.data)
    }

    render() {
        return (
            <SafeAreaView>
                {
                    (
                        this.props.data.isLogin ?
                            <TouchableOpacity style={styles.button}><Text style={styles.text}>Logout</Text></TouchableOpacity>

                            :

                            <View>
                                <TouchableOpacity style={styles.button} onPress={() => { this.props.navigation.navigate("Login") }}><Text style={styles.text}>Login</Text></TouchableOpacity>
                                <TouchableOpacity style={styles.button} onPress={() => { this.props.navigation.navigate("Register") }}><Text style={styles.text}>Registrasi</Text></TouchableOpacity>
                            </View>
                    )
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    button:{
        padding:10,
    },
    text:{
        textAlign:'center',
        borderWidth:5,
    }
  });

const mapStateToProps = (state) => ({
    data: state.userReducer
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, {})(Home)
